#include <stdio.h>
#include <stdlib.h>

struct lista{
    int info;
    struct lista* prox;
};

typedef struct lista Lista;

//fun��o de inicializa��o
Lista * inicializa()
{
    return NULL;
}

//fun��o de inser��o
Lista * insere (Lista* l,int i)
{
    Lista * novo = (Lista *) malloc(sizeof(Lista));
    novo->info = i;
    novo->prox = l;
    return novo;
}

void imprime(Lista * l)
{
    Lista* p; // vari�vel auxiliar para percorrer a lista e n�o perder a vari�vel que � o in�cio da lista
    for(p = l; p != NULL; p = p->prox)
        printf("info = %d\n",p->info);
}
//fun��o que verifica se a lista est� vazia, se sim, retorna 1, se n�o, retorna 0;
int vazia(Lista * l)
{
    if (l == NULL)
        return 1;
    else
        return 0;
    //ou

    /*
        return (l == NULL);
    */
}

//fun��o de busca
Lista * busca (Lista * l, int v)
{
    Lista * p;
    for (p = l; p!=NULL; p=p->prox)
    {
        if (p->info == v)
            return p;
    }
    return NULL; // n�o achou o elemnto na lista

}

//fun��o retira
Lista* retira (Lista* l, int v)
{
    Lista* ant = NULL; //pointer para elemento anterior
    Lista * p = l; //pointer para correr pela lista

    //procura elemento na lista e guarda o anterior
    while(p != NULL && p-> info != v)
    {
        ant = p;
        p = p->prox;
    }
    //verifica se achou elemento
    if(p==NULL)
        return l; //n�o achou, volta a lista original
    //caso contr�rio, retira elemento
    if(ant == NULL)
    {
        //retira elemento do in�cio
        l = p->prox;
    }
    else
    {
        //retira elemento do meio da lista
        ant->prox = p->prox;
    }
    free(p);
    return l;
}

/*
int main()
{
    int v = 0;
    Lista* l; //declara lista n�o inicializada;
    Lista* p;
    l = inicializa();
    l = insere(l,23);
    l = insere(l,45);
    imprime(l);
    printf("Procurar por: ");
    scanf("%d", &v);
    p = busca(l,v);
    if(p!= NULL)
    {
        printf("Encontrei:\ninfo=%d",p->info);
    }
    else
    {
        printf("Valor n�o encontrado!");
    }
    return 0;
}
*/

int main()
{
    int continua=0;
    int valor=0;
    Lista* l;
    l = inicializa();
    printf("Insira valores maiores que 0 na lista.\nQualquer valor abaixo de 0 ira printar os valores da lista.\n");
    do
    {
        printf("Insira um valor:\n");
        scanf("%d",&valor);
        if(valor>0)
            l = insere(l,valor);
    }
    while(valor>0);
    imprime(l);
    l = retira(l,53);
    imprime(l);
}
